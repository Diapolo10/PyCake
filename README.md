# PyCake
A module for Python 3.x. Contains silly functions for party fun!

Usage:

1. Copy the file cake.py on your Python3 modules folder

2. Import the module by adding one of the following to your program:

    - ``` import cake```

    - ``` import cake.function_of_your_choice ```

    - ``` from cake import function_of_your_choice```

    - ``` from cake import *```

3. Depending on how you imported the module, you can use the functions like this:

 ```cake.function_of_your_choice(*args, **kwargs)```

Documentation:

PyCake currently contains one function.

```python
cake.cake(delay=0.5, charspeed=0.1, caketype=0)
# delay - the amount of time it takes for the function to proceed from one string to another, smaller is faster, 0 is instant
# charspeed - defines how quickly the string characters are shown, smaller is faster, 0 is instant
# caketype - defines which cake/message will be used. Currently values go from 0 to 3```
